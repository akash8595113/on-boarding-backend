import mongoose from "mongoose";

const userSchema = new mongoose.Schema(
  {
    account_type: String,
    strategy_name: String,
    fees_category: String,
    investment_category: String,
    address: String,
    country: String,
    count_account_holders: String,
    quantum_investment: String,
    investment_mode: String,
    applicant_name: String,
    applicant_contact_number: String,
    applicant_email_id: String,
    applicant_pan_no: String,
    applicant_dob: String,
    bank_preference: String,
    bank_name: String,
    account_no: String,
    account_title: String,
    IFSC_code: String,
    count_nominee: String,
    nominee_name: String,
    nominee_pan: String,
    nominee_email_id: String,
    nominee_contact_no: String,
    nominee_dob: String,
    nominee_relation: String,
    nominee_per: String,
    investment_experience: String,
    investment_style: String,
    reaction_on_portfolio_diversified: String,
    investment_objective: String,
    risk_tolerance: String,
    investment_horizon: String,
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("User", userSchema);
export default User;
