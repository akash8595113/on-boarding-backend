import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import User from "./userModel.js";
import dotenv from "dotenv";
import path from "path";
import { fileURLToPath } from 'url';



const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);


const app = express();
const PORT = process.env.PORT || 8080;

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, "../on-boarding-frontend/on-boarding-frontend/build")));
dotenv.config();

main().catch((err) => console.log(err));

async function main() {
  await mongoose.connect(process.env.MONGO_URL);
  console.log("Connected to DB");
}

app.get("/*", function (req, res) {
  res.sendFile(path.resolve(__dirname, "../on-boarding-frontend/on-boarding-frontend/build", "index.html"));
});

app.post("/api", async (req, res) => {
  const newUser = new User();
  newUser.account_type = req.body.account_type;
  newUser.strategy_name = req.body.strategy_name;
  newUser.fees_category = req.body.fees_category;
  newUser.investment_category = req.body.investment_category;
  newUser.address = req.body.address;
  newUser.country = req.body.country;
  newUser.count_account_holders = req.body.count_account_holders;
  newUser.quantum_investment = req.body.quantum_investment;
  newUser.investment_mode = req.body.investment_mode;
  newUser.applicant_name = req.body.applicant_name;
  newUser.applicant_contact_number = req.body.applicant_contact_number;
  newUser.applicant_email_id = req.body.applicant_email_id;
  newUser.applicant_pan_no = req.body.applicant_pan_no;
  newUser.applicant_dob = req.body.applicant_dob;
  newUser.bank_preference = req.body.bank_preference;
  newUser.bank_name = req.body.bank_name;
  newUser.account_no = req.body.account_no;
  newUser.account_title = req.body.account_title;
  newUser.IFSC_code = req.body.IFSC_code;
  newUser.count_nominee = req.body.count_nominee;
  newUser.nominee_name = req.body.nominee_name;
  newUser.nominee_pan = req.body.nominee_pan;
  newUser.nominee_email_id = req.body.nominee_email_id;
  newUser.nominee_contact_no = req.body.nominee_contact_no;
  newUser.nominee_dob = req.body.nominee_dob;
  newUser.nominee_relation = req.body.nominee_relation;
  newUser.nominee_per = req.body.nominee_per;
  newUser.investment_experience = req.body.investment_experience;
  newUser.investment_style = req.body.investment_style;
  newUser.reaction_on_portfolio_diversified =
    req.body.reaction_on_portfolio_diversified;
  newUser.investment_objective = req.body.investment_objective;
  newUser.risk_tolerance = req.body.risk_tolerance;
  newUser.investment_horizon = req.body.investment_horizon;

  const doc = await newUser.save();
  console.log(doc);
  res.json(doc);
});

app.listen(PORT, () => {
  console.log("Server Started");
});
